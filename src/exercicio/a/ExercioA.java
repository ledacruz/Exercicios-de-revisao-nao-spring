package exercicio.a;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class ExercioA {

	public static void main(String[] args) {
		String palavra = "";
		int contador = 0;
		Set<String> chaves;

		Scanner scanner = new Scanner(System.in);
		HashMap<String, Integer> lista = new HashMap<>();

		while (contador <= 5) {
			System.out.println("Digite uma palavra");
			palavra = scanner.nextLine();

			// se tem a palavra na lista, add 1 no valor
			if (lista.containsKey(palavra)) {
				lista.replace(palavra, (lista.get(palavra) + 1));
			} else {
				lista.put(palavra, 1);
			}

			chaves = lista.keySet();
			System.out.println("A lista de palavras eh:");
			for (String chave : chaves) {
				if (chave != null)
					System.out.println(chave + " " + lista.get(chave));
			}
			contador++;
		}

	}

}
